graph [
	directed 0
	node [
		id 0
		label "0"
		graphics [ 
			x 395.0971265995152
			y 237.0523091294441
			w 5
			h 5
		]
	]
	node [
		id 1
		label "1"
		graphics [ 
			x 344.07641946535404
			y 217.33041513982604
			w 5
			h 5
		]
	]
	node [
		id 2
		label "2"
		graphics [ 
			x 252.0867505801167
			y 225.157474539869
			w 5
			h 5
		]
	]
	node [
		id 3
		label "3"
		graphics [ 
			x 382.910769911426
			y 202.94619195983773
			w 5
			h 5
		]
	]
	node [
		id 4
		label "4"
		graphics [ 
			x 497.64507572785965
			y 338.8594564514238
			w 5
			h 5
		]
	]
	node [
		id 5
		label "5"
		graphics [ 
			x 551.0618278604088
			y 328.0887257554266
			w 5
			h 5
		]
	]
	node [
		id 6
		label "6"
		graphics [ 
			x 560.917124695891
			y 292.39822397360797
			w 5
			h 5
		]
	]
	node [
		id 7
		label "7"
		graphics [ 
			x 342.8072723724366
			y 282.35967270188087
			w 5
			h 5
		]
	]
	node [
		id 8
		label "8"
		graphics [ 
			x 235.02550745217587
			y 193.47906587093448
			w 5
			h 5
		]
	]
	node [
		id 9
		label "9"
		graphics [ 
			x 157.42814356984783
			y 284.20810286555593
			w 5
			h 5
		]
	]
	node [
		id 10
		label "10"
		graphics [ 
			x 509.7617350519962
			y 292.45968329074617
			w 5
			h 5
		]
	]
	node [
		id 11
		label "11"
		graphics [ 
			x 400.80662549743107
			y 232.59461880335056
			w 5
			h 5
		]
	]
	node [
		id 12
		label "12"
		graphics [ 
			x 493.25484161491113
			y 192.94355543163172
			w 5
			h 5
		]
	]
	node [
		id 13
		label "13"
		graphics [ 
			x 301.51481490828377
			y 179.81426850413817
			w 5
			h 5
		]
	]
	node [
		id 14
		label "14"
		graphics [ 
			x 4.02308955786026
			y 97.86133950198415
			w 5
			h 5
		]
	]
	node [
		id 15
		label "15"
		graphics [ 
			x 169.17029590961323
			y 35.53807217738881
			w 5
			h 5
		]
	]
	node [
		id 16
		label "16"
		graphics [ 
			x 650.8253876683015
			y 349.66185541495764
			w 5
			h 5
		]
	]
	node [
		id 17
		label "17"
		graphics [ 
			x 448.4583737236077
			y 158.63867613124012
			w 5
			h 5
		]
	]
	node [
		id 18
		label "18"
		graphics [ 
			x 0.0
			y 164.87273662238516
			w 5
			h 5
		]
	]
	node [
		id 19
		label "19"
		graphics [ 
			x 297.13257346531907
			y 150.56103965303333
			w 5
			h 5
		]
	]
	node [
		id 20
		label "20"
		graphics [ 
			x 59.83528708830754
			y 230.20002557123902
			w 5
			h 5
		]
	]
	node [
		id 21
		label "21"
		graphics [ 
			x 397.52924187584415
			y 328.16103651448867
			w 5
			h 5
		]
	]
	node [
		id 22
		label "22"
		graphics [ 
			x 123.7535848477325
			y 22.091317873612677
			w 5
			h 5
		]
	]
	node [
		id 23
		label "23"
		graphics [ 
			x 56.52280239381166
			y 83.8219001610913
			w 5
			h 5
		]
	]
	node [
		id 24
		label "24"
		graphics [ 
			x 100.52837886420585
			y 56.86560467600823
			w 5
			h 5
		]
	]
	node [
		id 25
		label "25"
		graphics [ 
			x 79.94850699827481
			y 0.0
			w 5
			h 5
		]
	]
	node [
		id 26
		label "26"
		graphics [ 
			x 4.186774732968644
			y 200.6819370086623
			w 5
			h 5
		]
	]
	node [
		id 27
		label "27"
		graphics [ 
			x 108.31665221582853
			y 166.94950652115364
			w 5
			h 5
		]
	]
	node [
		id 28
		label "28"
		graphics [ 
			x 173.96337574181055
			y 211.24614006838598
			w 5
			h 5
		]
	]
	node [
		id 29
		label "29"
		graphics [ 
			x 29.916779239209887
			y 137.79489254501215
			w 5
			h 5
		]
	]
	node [
		id 30
		label "30"
		graphics [ 
			x 232.78131269779908
			y 147.0693561475909
			w 5
			h 5
		]
	]
	node [
		id 31
		label "31"
		graphics [ 
			x 203.92502914422326
			y 112.53626833947945
			w 5
			h 5
		]
	]
	node [
		id 32
		label "32"
		graphics [ 
			x 121.98467579458715
			y 129.30560124300453
			w 5
			h 5
		]
	]
	node [
		id 33
		label "33"
		graphics [ 
			x 137.30227074858033
			y 152.18258367734902
			w 5
			h 5
		]
	]
	edge [
		source 10
		target 5
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 15
		target 32
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 3
		target 13
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 23
		target 27
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 5
		target 16
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 33
		target 31
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 33
		target 22
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 29
		target 33
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 1
		target 30
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 33
		target 13
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 2
		target 32
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 33
		target 8
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 14
		target 32
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 18
		target 32
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 6
		target 16
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 24
		target 25
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 11
		target 0
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 31
		target 32
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 33
		target 32
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 1
		target 2
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 9
		target 2
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 28
		target 33
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 1
		target 0
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 6
		target 5
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 3
		target 2
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 3
		target 0
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 2
		target 7
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 5
		target 0
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 0
		target 7
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 17
		target 0
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 31
		target 0
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 13
		target 0
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 2
		target 27
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 33
		target 20
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 29
		target 32
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 19
		target 0
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 9
		target 33
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 14
		target 33
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 33
		target 18
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 3
		target 12
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 21
		target 1
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 23
		target 29
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 23
		target 25
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 30
		target 32
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 23
		target 33
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 22
		target 32
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 25
		target 31
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 33
		target 27
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 1
		target 13
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 17
		target 1
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 1
		target 19
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 20
		target 32
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 10
		target 4
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 33
		target 15
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 8
		target 32
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 2
		target 13
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 33
		target 19
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 24
		target 27
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 33
		target 30
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 21
		target 0
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 10
		target 0
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 26
		target 29
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 8
		target 30
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 1
		target 3
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 1
		target 7
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 6
		target 4
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 6
		target 0
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 3
		target 7
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 2
		target 8
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 2
		target 0
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 8
		target 0
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 4
		target 0
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 12
		target 0
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 23
		target 32
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 26
		target 33
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 28
		target 2
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 24
		target 31
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 28
		target 31
		graphics [
			type "line"
			width 1.0
		]
	]
]
