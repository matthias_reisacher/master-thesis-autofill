graph [
	directed 0
	node [
		id 1
		label "1"
		graphics [ 
			x 949.140645700088
			y 1128.0073444990423
			w 5
			h 5
		]
	]
	node [
		id 2
		label "2"
		graphics [ 
			x 962.428455950791
			y 1145.0574861409557
			w 5
			h 5
		]
	]
	node [
		id 3
		label "3"
		graphics [ 
			x 957.4283551568278
			y 1147.9719908327158
			w 5
			h 5
		]
	]
	node [
		id 4
		label "4"
		graphics [ 
			x 951.8341700595493
			y 1149.455378390124
			w 5
			h 5
		]
	]
	node [
		id 5
		label "5"
		graphics [ 
			x 946.0469062170637
			y 1149.4013157825414
			w 5
			h 5
		]
	]
	node [
		id 6
		label "6"
		graphics [ 
			x 940.4814095679144
			y 1147.8136783565549
			w 5
			h 5
		]
	]
	node [
		id 7
		label "7"
		graphics [ 
			x 935.5366292085523
			y 1144.806272041181
			w 5
			h 5
		]
	]
	node [
		id 8
		label "8"
		graphics [ 
			x 931.1238037417784
			y 1116.0630304912268
			w 5
			h 5
		]
	]
	node [
		id 9
		label "9"
		graphics [ 
			x 936.2504253872312
			y 1110.6546659866624
			w 5
			h 5
		]
	]
	node [
		id 10
		label "10"
		graphics [ 
			x 942.9089733745191
			y 1107.3085645520325
			w 5
			h 5
		]
	]
	node [
		id 11
		label "11"
		graphics [ 
			x 930.8392630570506
			y 1139.5109363129552
			w 5
			h 5
		]
	]
	node [
		id 12
		label "12"
		graphics [ 
			x 927.8424317090171
			y 1131.7031951532974
			w 5
			h 5
		]
	]
	node [
		id 13
		label "13"
		graphics [ 
			x 928.0335335715513
			y 1123.3422561974796
			w 5
			h 5
		]
	]
	node [
		id 14
		label "14"
		graphics [ 
			x 968.939999324627
			y 1136.6825290687473
			w 5
			h 5
		]
	]
	node [
		id 15
		label "15"
		graphics [ 
			x 969.7264450881812
			y 1121.4120359527203
			w 5
			h 5
		]
	]
	node [
		id 16
		label "16"
		graphics [ 
			x 947.5661040284763
			y 1106.4482617311612
			w 5
			h 5
		]
	]
	node [
		id 17
		label "17"
		graphics [ 
			x 949.566150676098
			y 1106.3950291028757
			w 5
			h 5
		]
	]
	node [
		id 18
		label "18"
		graphics [ 
			x 951.5625521222719
			y 1106.5269441175276
			w 5
			h 5
		]
	]
	node [
		id 19
		label "19"
		graphics [ 
			x 953.5382056635008
			y 1106.84287669009
			w 5
			h 5
		]
	]
	node [
		id 20
		label "20"
		graphics [ 
			x 955.4761863387284
			y 1107.340120300226
			w 5
			h 5
		]
	]
	node [
		id 21
		label "21"
		graphics [ 
			x 957.3598919215156
			y 1108.0144151784164
			w 5
			h 5
		]
	]
	node [
		id 22
		label "22"
		graphics [ 
			x 959.1731851474251
			y 1108.859984798404
			w 5
			h 5
		]
	]
	node [
		id 23
		label "23"
		graphics [ 
			x 960.9005319581865
			y 1109.8695853633446
			w 5
			h 5
		]
	]
	node [
		id 24
		label "24"
		graphics [ 
			x 962.5271345783335
			y 1111.0345678617148
			w 5
			h 5
		]
	]
	node [
		id 25
		label "25"
		graphics [ 
			x 964.0390582842783
			y 1112.3449521613682
			w 5
			h 5
		]
	]
	node [
		id 26
		label "26"
		graphics [ 
			x 965.4233507798276
			y 1113.7895125069901
			w 5
			h 5
		]
	]
	node [
		id 27
		label "27"
		graphics [ 
			x 904.6612405201072
			y 841.6669713253623
			w 5
			h 5
		]
	]
	node [
		id 28
		label "28"
		graphics [ 
			x 868.2648395622327
			y 560.5908460735595
			w 5
			h 5
		]
	]
	node [
		id 29
		label "29"
		graphics [ 
			x 831.1624024292164
			y 291.8916988820547
			w 5
			h 5
		]
	]
	node [
		id 30
		label "30"
		graphics [ 
			x 882.1847638856752
			y 137.23927664690336
			w 5
			h 5
		]
	]
	node [
		id 31
		label "31"
		graphics [ 
			x 910.5328409010936
			y 26.7062269736806
			w 5
			h 5
		]
	]
	node [
		id 32
		label "32"
		graphics [ 
			x 916.970512088179
			y 4.810950512571935
			w 5
			h 5
		]
	]
	node [
		id 33
		label "33"
		graphics [ 
			x 727.806777293481
			y 215.7964271611297
			w 5
			h 5
		]
	]
	node [
		id 34
		label "34"
		graphics [ 
			x 644.1386097458321
			y 151.9635688288115
			w 5
			h 5
		]
	]
	node [
		id 35
		label "35"
		graphics [ 
			x 627.8607168394847
			y 138.62077882537915
			w 5
			h 5
		]
	]
	node [
		id 36
		label "36"
		graphics [ 
			x 786.4674626211067
			y 131.0020037312538
			w 5
			h 5
		]
	]
	node [
		id 37
		label "37"
		graphics [ 
			x 746.8670944924343
			y 22.05294463751386
			w 5
			h 5
		]
	]
	node [
		id 38
		label "38"
		graphics [ 
			x 739.712299027276
			y 0.0
			w 5
			h 5
		]
	]
	node [
		id 39
		label "39"
		graphics [ 
			x 720.0910984914512
			y 1359.5148132257593
			w 5
			h 5
		]
	]
	node [
		id 40
		label "40"
		graphics [ 
			x 495.5464335117408
			y 1582.2094160754284
			w 5
			h 5
		]
	]
	node [
		id 41
		label "41"
		graphics [ 
			x 285.3867281379207
			y 1790.7371924340025
			w 5
			h 5
		]
	]
	node [
		id 42
		label "42"
		graphics [ 
			x 208.36425665733987
			y 1944.616236996061
			w 5
			h 5
		]
	]
	node [
		id 43
		label "43"
		graphics [ 
			x 154.55127682564898
			y 2044.7971386425809
			w 5
			h 5
		]
	]
	node [
		id 44
		label "44"
		graphics [ 
			x 143.0959845326447
			y 2064.44550428975
			w 5
			h 5
		]
	]
	node [
		id 45
		label "45"
		graphics [ 
			x 142.86201308105
			y 1907.1863171971384
			w 5
			h 5
		]
	]
	node [
		id 46
		label "46"
		graphics [ 
			x 46.57000728953699
			y 1988.3758890151228
			w 5
			h 5
		]
	]
	node [
		id 47
		label "47"
		graphics [ 
			x 26.756642790087426
			y 2003.9318031723035
			w 5
			h 5
		]
	]
	node [
		id 48
		label "48"
		graphics [ 
			x 160.10866816341195
			y 1830.8690516336603
			w 5
			h 5
		]
	]
	node [
		id 49
		label "49"
		graphics [ 
			x 57.31846828622702
			y 1869.0350150080055
			w 5
			h 5
		]
	]
	node [
		id 50
		label "50"
		graphics [ 
			x 36.506557130697274
			y 1875.9460924998064
			w 5
			h 5
		]
	]
	node [
		id 51
		label "51"
		graphics [ 
			x 713.1562356696331
			y 1034.4938940520992
			w 5
			h 5
		]
	]
	node [
		id 52
		label "52"
		graphics [ 
			x 488.2797206545483
			y 935.3391796863573
			w 5
			h 5
		]
	]
	node [
		id 53
		label "53"
		graphics [ 
			x 273.2254297098498
			y 839.302173906113
			w 5
			h 5
		]
	]
	node [
		id 54
		label "54"
		graphics [ 
			x 136.5315164179865
			y 760.6875305483027
			w 5
			h 5
		]
	]
	node [
		id 55
		label "55"
		graphics [ 
			x 36.02127665490707
			y 690.9741173848058
			w 5
			h 5
		]
	]
	node [
		id 56
		label "56"
		graphics [ 
			x 16.418066933328646
			y 676.3383769014117
			w 5
			h 5
		]
	]
	node [
		id 57
		label "57"
		graphics [ 
			x 220.29683183997213
			y 725.9728382121057
			w 5
			h 5
		]
	]
	node [
		id 58
		label "58"
		graphics [ 
			x 168.47130727113472
			y 634.9111043457851
			w 5
			h 5
		]
	]
	node [
		id 59
		label "59"
		graphics [ 
			x 158.74811823538312
			y 616.348115097335
			w 5
			h 5
		]
	]
	node [
		id 60
		label "60"
		graphics [ 
			x 132.25830841201923
			y 864.5624511622209
			w 5
			h 5
		]
	]
	node [
		id 61
		label "61"
		graphics [ 
			x 22.078027876971646
			y 872.4988893894661
			w 5
			h 5
		]
	]
	node [
		id 62
		label "62"
		graphics [ 
			x 0.0
			y 873.3161628347259
			w 5
			h 5
		]
	]
	node [
		id 63
		label "63"
		graphics [ 
			x 1057.20844752404
			y 1129.7908776888146
			w 5
			h 5
		]
	]
	node [
		id 64
		label "64"
		graphics [ 
			x 1177.9547271549536
			y 1119.4746011161367
			w 5
			h 5
		]
	]
	node [
		id 65
		label "65"
		graphics [ 
			x 1310.2800876221413
			y 1115.0755155730797
			w 5
			h 5
		]
	]
	node [
		id 66
		label "66"
		graphics [ 
			x 1410.2723010797372
			y 1037.0302893189632
			w 5
			h 5
		]
	]
	node [
		id 67
		label "67"
		graphics [ 
			x 1503.8149996644815
			y 996.5425445485753
			w 5
			h 5
		]
	]
	node [
		id 68
		label "68"
		graphics [ 
			x 1522.794743027544
			y 989.1028470226922
			w 5
			h 5
		]
	]
	node [
		id 69
		label "69"
		graphics [ 
			x 1395.7603657512514
			y 1123.8622744813058
			w 5
			h 5
		]
	]
	node [
		id 70
		label "70"
		graphics [ 
			x 1491.2665247311256
			y 1155.531236179744
			w 5
			h 5
		]
	]
	node [
		id 71
		label "71"
		graphics [ 
			x 1510.135074407915
			y 1162.5277935203487
			w 5
			h 5
		]
	]
	node [
		id 72
		label "72"
		graphics [ 
			x 1339.516523417426
			y 1225.480144571405
			w 5
			h 5
		]
	]
	node [
		id 73
		label "73"
		graphics [ 
			x 1377.2878433185488
			y 1311.8263533399147
			w 5
			h 5
		]
	]
	node [
		id 74
		label "74"
		graphics [ 
			x 1384.234817599628
			y 1329.3487151490074
			w 5
			h 5
		]
	]
	node [
		id 75
		label "75"
		graphics [ 
			x 1198.7677941642087
			y 1355.7583290260043
			w 5
			h 5
		]
	]
	node [
		id 76
		label "76"
		graphics [ 
			x 1457.425622698675
			y 1563.0231156910252
			w 5
			h 5
		]
	]
	node [
		id 77
		label "77"
		graphics [ 
			x 1710.1741642487855
			y 1757.7885570351957
			w 5
			h 5
		]
	]
	node [
		id 78
		label "78"
		graphics [ 
			x 1823.5024757349106
			y 1905.592972976143
			w 5
			h 5
		]
	]
	node [
		id 79
		label "79"
		graphics [ 
			x 1898.3332470061036
			y 1997.4201890704364
			w 5
			h 5
		]
	]
	node [
		id 80
		label "80"
		graphics [ 
			x 1912.6493395584862
			y 2016.2967558085486
			w 5
			h 5
		]
	]
	node [
		id 81
		label "81"
		graphics [ 
			x 1876.1122058245478
			y 1828.7462614975814
			w 5
			h 5
		]
	]
	node [
		id 82
		label "82"
		graphics [ 
			x 1978.885878074007
			y 1892.4059191877375
			w 5
			h 5
		]
	]
	node [
		id 83
		label "83"
		graphics [ 
			x 1998.9837531323142
			y 1905.8474446642356
			w 5
			h 5
		]
	]
	node [
		id 84
		label "84"
		graphics [ 
			x 1820.1516991378699
			y 1856.8336708823742
			w 5
			h 5
		]
	]
	node [
		id 85
		label "85"
		graphics [ 
			x 1912.7149500856144
			y 1943.0937213365544
			w 5
			h 5
		]
	]
	node [
		id 86
		label "86"
		graphics [ 
			x 1930.6142363995864
			y 1960.9813041432215
			w 5
			h 5
		]
	]
	node [
		id 87
		label "87"
		graphics [ 
			x 1156.775772530922
			y 950.8299177693307
			w 5
			h 5
		]
	]
	node [
		id 88
		label "88"
		graphics [ 
			x 1368.808724951059
			y 785.9524434268849
			w 5
			h 5
		]
	]
	node [
		id 89
		label "89"
		graphics [ 
			x 1572.296826371442
			y 634.4107578456985
			w 5
			h 5
		]
	]
	node [
		id 90
		label "90"
		graphics [ 
			x 1680.1849388334776
			y 520.7384240598701
			w 5
			h 5
		]
	]
	node [
		id 91
		label "91"
		graphics [ 
			x 1755.3919033806555
			y 431.50619458701317
			w 5
			h 5
		]
	]
	node [
		id 92
		label "92"
		graphics [ 
			x 1771.0469654595845
			y 414.19555730732986
			w 5
			h 5
		]
	]
	node [
		id 93
		label "93"
		graphics [ 
			x 1601.2743916189952
			y 509.07034485886425
			w 5
			h 5
		]
	]
	node [
		id 94
		label "94"
		graphics [ 
			x 1623.453262471966
			y 403.88138792720224
			w 5
			h 5
		]
	]
	node [
		id 95
		label "95"
		graphics [ 
			x 1628.620542819901
			y 383.011218481
			w 5
			h 5
		]
	]
	node [
		id 96
		label "96"
		graphics [ 
			x 1733.5642333436504
			y 611.9227300858975
			w 5
			h 5
		]
	]
	node [
		id 97
		label "97"
		graphics [ 
			x 1846.1598480300868
			y 589.2875076743444
			w 5
			h 5
		]
	]
	node [
		id 98
		label "98"
		graphics [ 
			x 1868.82324450011
			y 585.5491270019088
			w 5
			h 5
		]
	]
	edge [
		source 80
		target 79
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 65
		target 72
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 22
		target 1
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 1
		target 10
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 65
		target 69
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 40
		target 39
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 33
		target 29
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 1
		target 14
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 1
		target 26
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 1
		target 18
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 54
		target 55
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 93
		target 94
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 97
		target 98
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 58
		target 59
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 58
		target 57
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 66
		target 65
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 76
		target 77
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 64
		target 65
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 28
		target 29
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 83
		target 82
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 32
		target 31
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 94
		target 95
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 54
		target 53
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 61
		target 60
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 73
		target 72
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 90
		target 91
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 44
		target 43
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 46
		target 47
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 75
		target 76
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 42
		target 43
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 61
		target 62
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 37
		target 36
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 6
		target 1
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 1
		target 2
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 87
		target 88
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 49
		target 50
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 29
		target 36
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 75
		target 1
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 84
		target 77
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 1
		target 23
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 57
		target 53
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 1
		target 39
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 27
		target 1
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 1
		target 13
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 1
		target 63
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 60
		target 53
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 1
		target 17
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 41
		target 48
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 1
		target 3
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 1
		target 7
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 96
		target 89
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 41
		target 45
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 1
		target 12
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 1
		target 20
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 73
		target 74
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 1
		target 16
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 1
		target 24
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 49
		target 48
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 34
		target 35
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 81
		target 77
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 93
		target 89
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 64
		target 63
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 31
		target 30
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 78
		target 79
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 70
		target 71
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 27
		target 28
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 88
		target 89
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 34
		target 33
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 40
		target 41
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 52
		target 53
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 46
		target 45
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 56
		target 55
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 41
		target 42
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 67
		target 68
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 84
		target 85
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 96
		target 97
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 37
		target 38
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 77
		target 78
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 66
		target 67
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 92
		target 91
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 86
		target 85
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 51
		target 52
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 81
		target 82
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 1
		target 4
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 1
		target 8
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 87
		target 1
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 90
		target 89
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 70
		target 69
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 11
		target 1
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 1
		target 21
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 1
		target 15
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 1
		target 25
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 1
		target 51
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 1
		target 19
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 5
		target 1
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 1
		target 9
		graphics [
			type "line"
			width 1.0
		]
	]
	edge [
		source 29
		target 30
		graphics [
			type "line"
			width 1.0
		]
	]
]
