extern crate env_logger;
extern crate futures;
extern crate grpcio;
extern crate grpcio_proto;
#[macro_use]
extern crate log;
extern crate base64;
extern crate protobuf;

use std::env;
use std::fs::{read_dir, read_to_string, DirEntry};
use std::path::Path;

use env_logger::Builder;

use grpc::client_backend::BackendClient;

mod grpc;

type BoxResult<T> = Result<T, Box<dyn std::error::Error>>;

static ENV_LOG_LEVEL: &str = "LOG_LEVEL";

fn main() -> BoxResult<()> {
    Builder::new()
        .parse_filters(&env::var(ENV_LOG_LEVEL).unwrap())
        .init();

    let mut path = env::current_dir()?;
    path.push("data");
    path.push("regular");
    info!(
        "Processing files in folder '{}'",
        path.canonicalize()?
            .as_path()
            .as_os_str()
            .to_str()
            .ok_or("Could not navigate to data directory")?
    );

    process_dir(path.as_path(), &process_file)?;

    Ok(())
}

/// Search for files in the given directory and its subdirectories.
fn process_dir(dir: &Path, cb: &dyn Fn(&DirEntry) -> BoxResult<()>) -> BoxResult<()> {
    if dir.is_dir() {
        for entry in read_dir(dir)? {
            let entry = entry?;
            let path = entry.path();

            if path.is_dir() {
                process_dir(&path, cb)?;
            } else if let Err(error) = cb(&entry) {
                error!("Error - ignoring entity: {}", error)
            }
        }
    }

    Ok(())
}

/// Parse file content and upload to backend
fn process_file(entity: &DirEntry) -> BoxResult<()> {
    let file_name = entity.file_name();
    let file_name = file_name.to_str().ok_or("Could get filename")?;
    info!("Processing file '{}'", file_name);

    let content = read_to_string(entity.path())?;
    debug!("Successfully parsed file with length {}", content.len());

    let stemmed_name = entity.path();
    let stemmed_name = stemmed_name
        .file_stem()
        .ok_or("Could stem filename")?
        .to_str()
        .ok_or("Could not get stemmed filename")?;

    let extension = entity.path();
    let extension = extension
        .extension()
        .ok_or("Could not extract file extension")?
        .to_str()
        .ok_or("Could not get file extension")?;

    let client = BackendClient::new()?;
    client.upload_file(stemmed_name, file_name, extension, &content)?;

    Ok(())
}
