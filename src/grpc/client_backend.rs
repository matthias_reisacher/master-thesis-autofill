use std::error::Error;
use std::sync::Arc;

use base64::encode;
use grpcio::{ChannelBuilder, EnvBuilder};

use grpc::create_client_url;
use grpc::graph::CreateGraphRequest;
use grpc::graph_grpc::GraphServiceClient;
use grpc::graphpage::{ChunkUploadRequest, FinalizeUploadRequest, InitUploadRequest};
use grpc::graphpage_grpc::GraphPageServiceClient;
use BoxResult;

static ENV_BACKEND_HOST: &str = "BACKEND_HOST";
static ENV_BACKEND_PORT: &str = "BACKEND_PORT";

pub struct BackendClient {
    graph_client: GraphServiceClient,
    graph_page_client: GraphPageServiceClient,
}

impl BackendClient {
    pub fn new() -> BoxResult<Self> {
        let url = create_client_url(ENV_BACKEND_HOST, ENV_BACKEND_PORT)?;
        debug!("Backend URL is '{}'", url);

        let env = Arc::new(EnvBuilder::new().build());
        let ch = ChannelBuilder::new(env).connect(url.as_str());
        let graph_client = GraphServiceClient::new(ch);

        let env = Arc::new(EnvBuilder::new().build());
        let ch = ChannelBuilder::new(env).connect(url.as_str());
        let graph_page_client = GraphPageServiceClient::new(ch);

        Ok(BackendClient {
            graph_client,
            graph_page_client,
        })
    }

    /// Create a new graph and upload the given data to the backend.
    pub fn upload_file(
        &self,
        graph_name: &str,
        file_name: &str,
        file_extension: &str,
        content: &str,
    ) -> BoxResult<()> {
        // Create new graph
        let graph_id = self.create_graph(graph_name, file_name)?;

        // Encode file data for transmission
        let content = encode(content);
        let data = content.into_bytes();

        // Upload graph page
        let upload_id =
            self.init_upload(graph_id, &file_name, &file_extension, data.len() as u64)?;
        self.chunk_upload(&upload_id, 1, data)?;
        self.finalize_upload(&upload_id, 1)
    }

    fn create_graph(&self, name: &str, description: &str) -> BoxResult<u64> {
        let mut req = CreateGraphRequest::new();
        req.set_name(name.to_string());
        req.set_description(description.to_string());

        self.graph_client
            .create_graph(&req)
            .map_err(|e| e.into())
            .and_then(|res| {
                debug!(
                    "Created graph '{}' with id {} successfully.",
                    name,
                    res.get_graph_id()
                );
                Ok(res.get_graph_id())
            })
            .map_err(|e: Box<dyn Error>| {
                error!("Error during graph creation: {}", e);
                e
            })
    }

    fn init_upload(
        &self,
        graph_id: u64,
        name: &str,
        extension: &str,
        size: u64,
    ) -> BoxResult<String> {
        let mut req = InitUploadRequest::new();
        req.set_graph_id(graph_id);
        req.set_name(name.to_string());
        req.set_field_type(extension.to_string());
        req.set_size(size);

        self.graph_page_client
            .init_upload(&req)
            .map_err(|e| e.into())
            .and_then(|res| {
                let upload_id = res.get_id().to_string();
                debug!("Initialized file upload with id {}.", res.get_id());
                Ok(upload_id)
            })
            .map_err(|e: Box<dyn Error>| {
                error!("Error during upload initialization: {}", e);
                e
            })
    }

    fn chunk_upload(&self, id: &str, number: u32, content: Vec<u8>) -> BoxResult<()> {
        let mut req = ChunkUploadRequest::new();
        req.set_id(id.to_string());
        req.set_number(number);
        req.set_chunk(content);

        self.graph_page_client
            .chunk_upload(&req)
            .map_err(|e| e.into())
            .and_then(|_| {
                debug!("Chunk uploaded successfully.");
                Ok(())
            })
            .map_err(|e: Box<dyn Error>| {
                error!("Error during chunk upload: {}", e);
                e
            })
    }

    fn finalize_upload(&self, id: &str, counter: u32) -> BoxResult<()> {
        let mut req = FinalizeUploadRequest::new();
        req.set_id(id.to_string());
        req.set_number_of_chunks(counter);

        self.graph_page_client
            .finalize_upload(&req)
            .map_err(|e| e.into())
            .and_then(|_| {
                debug!("Finalized upload with id {}.", id);
                Ok(())
            })
            .map_err(|e: Box<dyn Error>| {
                error!("Error during upload finalization: {}", e);
                e
            })
    }
}
