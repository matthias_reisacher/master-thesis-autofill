use std::env;

use BoxResult;

mod empty;
mod graph;
mod graph_grpc;
mod graphpage;
mod graphpage_grpc;
pub mod client_backend;

fn create_client_url(host: &str, port: &str) -> BoxResult<String> {
    let host = env::var(host)?;
    let port = env::var(port)?;

    Ok(format!("{}:{}", host, port))
}
